const { defineConfig } = require("cypress");
const createBundler = require("@bahmutov/cypress-esbuild-preprocessor");
const { addCucumberPreprocessorPlugin } = require("@badeball/cypress-cucumber-preprocessor");
const { createEsbuildPlugin } = require("@badeball/cypress-cucumber-preprocessor/esbuild");

module.exports = defineConfig({
  env: {
    USER_STANDARD: 'standard_user',
    USER_LOCKED: 'locked_out_user',
    USER_PROBLEM: 'problem_user',
    USER_PERFORMANCE: 'performance_glitch_user',
    USER_ERROR: 'error_user',
    PASSWORD: 'secret_sauce',
  },
  e2e: {
    chromeWebSecurity: false,
    supportFile: false,
    specPattern: 'cypress/integration/*/{*.cy.js,*.feature}',
    async setupNodeEvents(on, config) {
      await addCucumberPreprocessorPlugin(on, config);

      on(
        "file:preprocessor",
        createBundler({
          plugins: [createEsbuildPlugin(config)],
        })
      );
      on('task', {
        log(message) {
          console.log(message);
          return null;
        },
      });
      return config;
    },
  },
});

