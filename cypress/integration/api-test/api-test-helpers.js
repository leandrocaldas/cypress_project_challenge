// Make an API request and handle the response
export function makeRequest(apiUrl, category, expectedCount, retryCount = 0) {
    cy.request({ url: apiUrl, failOnStatusCode: false }).then((response) => {
      // This step is crucial, especially for handling 429 status codes.
      // It will retry the request a maximum of 15 times if a 429 status is encountered.
      if (response.status === 429 && retryCount < 15) {
        cy.task('log', 'Retrying the request after a 429 status...');
        return makeRequest(apiUrl, category, expectedCount, retryCount + 1);
      } else if (response.status === 200) {
        cy.wrap(response).as('apiResponse');
      } else {
        handleUnexpectedResponse(response);
      }
    });
  }  
  
  // Handle a successful response and assert against the expected category and count
  export function handleCategoryResponse(response, category, expectedCount) {
    const responseData = response.body.entries;
    const filteredObjects = responseData.filter((entry) => {
      return entry.Category === category;
    });
  
    cy.wrap(filteredObjects.length).should('be.eq', expectedCount);
  
    filteredObjects.forEach((entry, index) => {
      cy.task('log', `Object ${index + 1}: ${JSON.stringify(entry)}`);
    });
  }
  
  // Handle unexpected responses
  export function handleUnexpectedResponse(response) {
    cy.task('log',`Unexpected response status: ${response.status}`);
  }
  