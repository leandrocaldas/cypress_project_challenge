import {
  makeRequest,
  handleCategoryResponse,
} from './api-test-helpers';

describe('API Test', () => {
  it('should call the API, find objects with Category: Authentication & Authorization, and print them', () => {
    const apiUrl = 'https://api.publicapis.org/entries';
    const category = 'Authentication & Authorization';
    const expectedCount = 7;

    makeRequest(apiUrl, category, expectedCount);
    cy.get('@apiResponse').then((response) => {
      handleCategoryResponse(response, category, expectedCount);
    });
  });
});
