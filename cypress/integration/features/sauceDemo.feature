Feature: Sauce Demo UI Tests

  Scenario: Verify items are sorted by Name "<order>"
    Given I am on the "<website>" website
    When I log in with "<user>" user
    Then I can see the order is "<order>"
    And I should verify that the items are sorted by "<order>"

    Examples:
    |website                   |user       |order|
    |https://www.saucedemo.com |standard   |az   |

  Scenario: Change items sorted order by Name "<order>"
    Given I am on the "<website>" website
    When I log in with "<user>" user
    And I change the sorting to Name "<order>"
    Then I can see the order is "<order>"
    And I should verify that the items are sorted by "<order>"

    Examples:
    |website                   |user       |order|
    |https://www.saucedemo.com |standard   |za   |