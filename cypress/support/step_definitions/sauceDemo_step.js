import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor';
import SauceDemoPage from '../../pageObject/sauceDemoPage';
import LoginPage from '../../pageObject/loginPage';
import { LOGIN_USERS } from '../../fixture/credentials';

Given('I am on the {string} website', (website) => {
  LoginPage.visit(website);
});

When('I log in with {string} user', (user) => {
  LoginPage.login(LOGIN_USERS[user.toUpperCase()], LOGIN_USERS.PASSWORD);
});

When('I change the sorting to Name {string}', (order) => {
  SauceDemoPage.selectSortOption(order);
});

Then('I can see the order is {string}', (order) => {
  SauceDemoPage.isCurrentSortOption(order);
});

Then('I should verify that the items are sorted by {string}', (order) => {
  SauceDemoPage.verifyProductsSortedOrder(order);
});

