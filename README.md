# Cypress Test Project

This project demonstrates how to run automated tests using Cypress for both API and UI tests in a web application. The UI tests are performed on the Sauce Demo website, while the API tests make requests to the public [publicapis.org](https://api.publicapis.org/) API.

## Prerequisites

Before running the tests, make sure you have Node.js and npm installed on your system. Also, ensure that the project dependencies have been installed by running the following command in the project's root directory:

```bash
npm install
```

## Running UI Tests local

### Tests on the Sauce Demo Website

To run the UI tests on the Sauce Demo website, follow the steps below:

```bash
npm run cy:ui
```

This command will execute the UI tests in electron.

## Running API Tests

To run the API tests, follow the steps below:

```bash
npm run cy:api
```
This command will make a request to the public API, find objects with the category "Authentication & Authorization," and print the results to the console.

This project includes a Cypress test for calling the [Public APIs](https://public-apis.io/) service and filtering objects with the "Authentication & Authorization" category.

**Note**: Please be aware that the API's responsiveness may vary depending on the day and time. The service's availability is subject to fluctuations.

### Handling API Response Status 429 (Rate Limit Exceeded)
A workaround has been implemented to handle cases where the API responds with a 429 status code. This ensures that the test continues to run without failures, even when the API returns a 429 status. While this approach is not considered a best practice, it guarantees test stability, particularly in scenarios where the API's availability may vary

## Running bouth

To run the API and UI tests, follow the steps below:

```bash
npm run cy:run
```

## Configuration

The project is configured using the `cypress.config.json` file, which contains environment variables for different user types and Cypress settings.

### Running the CI/CD Pipeline in GitLab

Our CI/CD pipeline is set up to automatically run on every new code change pushed to this repository. This ensures that your changes are continuously integrated and tested.

#### Automatic Execution
- Any code changes you push to this repository will trigger the pipeline to run automatically. The results will be visible in the GitLab CI/CD interface.

#### Manual Execution
If you wish to manually run the pipeline in GitLab, you can follow these steps:
1. Navigate to the "CI/CD" section in your GitLab project.
2. Click on "Pipelines" to view all the pipeline runs.
3. In the "Pipelines" view, you can click the "Run Pipeline" button to initiate a manual run of the pipeline. This is useful for testing or running the pipeline without waiting for an automatic trigger.

By leveraging CI/CD, we ensure that code changes are thoroughly tested and integrated into the project with ease and efficiency.



## Project Structure

- `cypress/`: Contains all tests, with separate subdirectories for API tests and UI tests.
- `cypress/pageObject/`: Contains page object files for UI tests.
- `cypress/fixture/`: Contains login credentials information.
- `cypress/integration/`:  Contains api tests and UI features.
- `cypress/support/step_definitions`:  Contains tests steps.
